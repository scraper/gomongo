package gomongo

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"time"
	discover "gitlab.com/scraper/goservicediscover"
	logger "gitlab.com/scraper/gojsonlogger"
)

type MongodDbOption struct {
	Address string
	Port    int
}

type MongoDBConnection struct {
	Session                *mgo.Session
	Log                    logger.Logger
	Discover               discover.ServiceDiscover
	Index                  *mgo.Index
	Option                 *MongodDbOption
	DatabaseName           string
	DatabaseCollectionName string
}

func NewMongoDbConnection(option *MongodDbOption) *MongoDBConnection {
	var obj *MongoDBConnection
	obj = &MongoDBConnection{
		Option: &MongodDbOption{Address:"localhost", Port: 27017},
		Session: nil,
		Index: nil,
	}

	if option != nil {
		*obj.Option = *option
	}

	return obj
}

func (connection *MongoDBConnection) Connect() error {

	url := connection.GetServiceAddress()
	session, err := mgo.Dial(url)

	if err == nil {
		if connection.Log != nil {
			connection.Log.Info("Connected to MongoDB")
		}
		connection.Session = session
		if connection.Index != nil {
			connection.CreateIndex(*connection.Index)
		}
	}

	return err
}

func (connection *MongoDBConnection) CreateIndex(index mgo.Index) {

	if connection.Session != nil {
		session := connection.Session.Copy()
		col := session.DB(connection.DatabaseName).C(connection.DatabaseCollectionName)
		col.EnsureIndex(index)
		defer session.Close()
	}
}

func (connection *MongoDBConnection) Reconnect(pollTimeInSeconds int) {

	if connection.Log != nil {
		connection.Log.Info(fmt.Sprintf("Retry connecting to MongoDB every %d seconds",
			pollTimeInSeconds))
	}
	for {
		<-time.After(time.Duration(pollTimeInSeconds) * time.Second)
		err := connection.Connect()
		if err == nil {
			return
		}
	}
}

func (connection *MongoDBConnection) GetServiceAddress() string {

	var mongoServerUrl = fmt.Sprintf("mongodb://%s:%d/%s", connection.Option.Address,
		connection.Option.Port, connection.DatabaseName)
	services, getServiceError := connection.Discover.GetServices("mongodb")
	if getServiceError != nil {
		connection.Log.Error(fmt.Sprintf("Could not discover MongoDB service = %s",
			getServiceError.Error()))
	} else {
		if len(services) > 0 {

			connection.Log.Info(fmt.Sprintf("Discovered MongoDB service: %s:%d",
				services[0].Address, services[0].Port))

			mongoServerUrl = fmt.Sprintf("mongodb://%s:%d/%s",
				services[0].Address, services[0].Port, connection.DatabaseName)
		}
	}
	return mongoServerUrl
}
